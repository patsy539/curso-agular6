var express = require("express"), cors = require('cors');
var app = express(); // inicializando un servidor web
app.use(express.json()); // le decimos que use json
app.use(cors());

app.listen(3000, () => console.log("Server running on port 3000")); // cuando empeze a escucahr el puerto 3000 nos dara esto en consola

var ciudades = [ "Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile", "Mexico", "NY" ];
// se configura 3 rutas, nos retornara el array
app.get ("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1 )));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos)); // ruta my
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

app.get("/api/translation", (req, res,next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang }
]));