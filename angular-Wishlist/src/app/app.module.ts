import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import {RouterModule, Routes} from '@angular/router'; 
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StoreModule as NgRxStoreModule, ActionReducerMap, Store} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects'
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

import './../polyfills'

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { DestinosApiClient } from './models/destinos-api-client.model';
import {FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesState, 
  reducerDestinosViajes, 
  intializeDestinosViajesState, 
  DestinosViajesEffects,
  InitMyDataAction
} from './models/destinos-viajes.state.model';
import { environment } from 'src/environments/environment.prod';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuariologueadoGuard } from './guards/usuario-logueado/usuariologueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import {HttpClientModule, HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import Dexie from 'dexie';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { DestinoViaje } from './models/DestinoViaje.model';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';




//app congif
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000' // aqui puede una variable  que tome el valor de un archivo de configuracion
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// init routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentComponent },
  { path: ':id', component: VuelosDetalleComponentComponent },
];


const routes: Routes =[
  {path:'', redirectTo: 'home', pathMatch: 'full'},
  {path:'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent },
  //{path:'destino', component: DestinoDetalleComponent}, //  destino/:id
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuariologueadoGuard ]
  },
  {
    path: 'vuelos', // cuando accesa a vuelo se cargara Vueloscomponent 
    component: VuelosComponentComponent,
    canActivate: [ UsuariologueadoGuard ], // y solo podra acceder si esta logueado
    children: childrenRoutesVuelos // y tendra rutas hijas
  }
]; // fin Routing

//redux init
export interface AppState {
  destinos: DestinosViajesState;
}
 const reducers: ActionReducerMap<AppState> = {
   destinos: reducerDestinosViajes
 };

 let reducersInitialState = {
    destinos: intializeDestinosViajesState()
 }; // fin redux init

//app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.intializeDestinosViajesState();
}
@Injectable()
class AppLoadService {
  constructor( private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({ 'x-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
} // end app init


//dexie DB
// es el objeto que guardara la informacion
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}
@Injectable ({
  providedIn: 'root'
})

export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number >;
  transalations: Dexie.Table<Translation, number>;
  constructor() {
         super('MyDatabase');
         this.version(1).stores({   // creamo la 1era version de la abse de datos que contendra 3 datos, id, nombre y un URL
                 destinos: '++id, nombre, imagenUrl',
          });
          this.version(2).stores({   // creamo la 2ndaversion de la abse de datos que contendra 3 datos, id, nombre y un URL
                destinos: '++id, lang, key, value',
                transalations: '++id, lang, key, value'
          });
  }
}
export const db = new MyDatabase();
//end dixie DB

// i18n inicio
class TranslationLoader implements  TranslationLoader {
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.transalations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.transalations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traduccciones) => {
                                        console.log('traducciones cargadas');
                                        console.log(traduccciones);
                                        return traduccciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}

// i18n fin


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot( reducers, { initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument (),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule 
  ],
  providers: [ 
   AuthService, UsuariologueadoGuard, //  DestinosApiClient,
   { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
   AppLoadService,
   { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }, // factory una fucnion que retornara un objeto que hara tareas cuando 
   MyDatabase
   // se inicialize la aplicacion
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule).then(ref => {
  // Ensure Angular destroys itself on hot reloads.
  if (window['ngRef']) {
    window['ngRef'].destroy();
  }
  window['ngRef'] = ref;

  // Otherise, log the boot error
}).catch(err => console.error(err));