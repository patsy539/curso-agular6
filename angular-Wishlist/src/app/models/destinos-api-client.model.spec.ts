import{
    reducerDestinosViajes,
    DestinosViajesState,
    intializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
} from './destinos-viajes.state.model';

import {DestinoViaje}  from './DestinoViaje.model';


describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        //setup cuando se armar los objetos para testear inicializa
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']); // se ejecuta cuando se carga la pagina
        // action acciones sobre el codigo un edo previo y una accion
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // assert para hacerlo interactuar se envian ambos al reducuer y lo retorna en el assert, osea ejecuta el reducer
        // assertions se validan las salidas se verifica
        expect(newState.items.length).toEqual(2); //--> se espera que el nvo estado haya creado dso destinos
        expect(newState.items[0].nombre).toEqual('destino 1'); // y que tenga un  nombre destino 1

        // el tear down son las accioens para voler atras
      
    });
    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url')); //
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1); // se verifica que items tenga una longitud de unoa
        expect(newState.items[0].nombre).toEqual('barcelona'); // y que el item cero sea igual a barcelona
    });
});

