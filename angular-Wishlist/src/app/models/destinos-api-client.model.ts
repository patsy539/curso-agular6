import { DestinoViaje } from './DestinoViaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, MyDatabase, db } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes.state.model';
import { HttpClientModule, HttpHeaders, HttpResponse, HttpClient, HttpRequest } from '@angular/common/http';
//import { HttpClient } from 'selenium-webdriver/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];
   // current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor( 
      private store: Store<AppState>,
      @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
      private http: HttpClient
    ){
       // this.destinos =[];
        this.store
        .select( state => state.destinos)
        .subscribe((data) => {
          console.log('destinos sub store');
          console.log(data);
          this.destinos = data.items;
        });
        this.store
          .subscribe((data) => {
            console.log('all store');
            console.log(data);
          })
    }
    //add(d:DestinoViaje){
      //  this.store.dispatch( new NuevoDestinoAction(d));
      // // this.destinos.push(d);
   // }

   // crea un request y nos suscribos ala repsuesta y enviamos infomracion(POST) a a una url  pasando parametros like el nombre
   add(d: DestinoViaje){
     const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
     const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, {headers: headers });
     this.http.request(req).subscribe((data: HttpResponse<{}>) => {
       if (data.status === 200){ // es un ok, proceso la info de manera correcta
         this.store.dispatch(new NuevoDestinoAction(d)); //se agrega el nuevo destino
         const myDb = db;
         myDb.destinos.add(d);
         console.log('todos los destinos de la dbl');
         myDb.destinos.toArray().then(destinos => console.log(destinos))
       }
     });
   }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    getById(id:String): DestinoViaje{
       // return this.destinos.filter(function(d){return d.isSelected.toString() === id; })[0];
        return this.destinos.filter(function(d) {return d.id.toString() === id; })[0];
    }
    elegir(d: DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }
       // this.destinos.forEach( x => x.setSelected(false));
       // d.setSelected(true);
      //  this.current.next(d);
    
    
    //encapsular
   // subscribeOnChange(fn){
     //   this.current.subscribe(fn);
   // }
}
