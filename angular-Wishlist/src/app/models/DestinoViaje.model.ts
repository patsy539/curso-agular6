import {v4 as uuid} from 'uuid';
export class DestinoViaje {

    private selected: boolean; 
    public servicios: string[];
    id = uuid();
    //public votes =0;

    constructor(public nombre: string, public imageUrl: string, public votes: number = 0){
        this.servicios =['desayuno', 'cena'];
    }

    isSelected(): boolean { // para saber si esta seleccioando o no
        return this.selected;
    }
    setSelected (s: boolean) { // para encapsular el acceso ala variable
        this.selected = s;
    } 

    voteUp(): any {
        this.votes++;
    }

    voteDown(): any{
        this.votes--;
    }
    // nombre:string;
   // imageUrl:string;

   // constructor(n:string, u:string){
      //  this.nombre = n;
      //  this.imageUrl =u;
    //}
}
