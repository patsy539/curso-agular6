import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/DestinoViaje.model'; // los modelos se tienen que importar con ../ subimos un nivel
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction, ElegidoFavoritoAction } from  '../../models/destinos-viajes.state.model'; //'./../../models/destinos-viajes.state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';
//Input es para poder recibir parametros
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style ({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('3s')
      ]),
    ]
    )
  ]
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje; // nombre es susceptible de ser pasado como parametro en el tag app destino-viaje
  @Input('idx') position: number;
  @HostBinding('attr.class') 	cssClass = "col-md-4"; // vinculacion de un atributo del tag el cual se nvuelve contenigo
  @Output() onClicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) { 
    this.onClicked = new EventEmitter(); // le cargamso un evento a la propiedad cliked
  }

  ngOnInit() {
  }

  ir() {
    // this.store.dispatch(new ElegidoFavoritoAction(this.destino));
    this.onClicked.emit(this.destino); // que destino fue clikeado
    return false;
  }

  voteUp() {
    this.store.dispatch( new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch( new VoteDownAction(this.destino));
    return false;
  }
}
