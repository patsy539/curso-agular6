import { Component, OnInit, EventEmitter, Output, } from '@angular/core';
import { DestinoViaje } from  './../../models/DestinoViaje.model'; // los modelos se tienen que importar con ../ subimos un nivel
import { DestinosApiClient} from  './../../models/destinos-api-client.model'; //'./../../models/destinos-api-client.model'
import { Store } from '@ngrx/store';
import {AppState} from  './../../app.module' // './../../app.module';
//import { ElegidoFavoritoAction, NuevoDestinoAction } from  './../../models/destinos-viajes.state.model';  // '../../../models/destinos-viajes.state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:  EventEmitter <DestinoViaje>;
  updates: string[];
  all;
  // destinos: DestinoViaje[]; // nuestro nuevo array de DEstino (es el dominio)
  // destinos: string[]; ventor de tipo string
  // dominio nuestor modelo es el destino de viaje
  constructor(
    private destinosApiClient:DestinosApiClient,
    private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];  // inicializado como vacio
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d !=null){
        this.updates.push('Se ha elegido a '+ d.nombre);
      }
    //this.destinosApiClient.subscribeOnChange((d: DestinoViaje) =>{
    });
    store.select( state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregado(d: DestinoViaje) { 
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
    e.setSelected(true);
   // this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }

  // guardar(nombre:string, url:string): boolean { // retorna un valor boleano
  //  this.destinos.push(new DestinoViaje(nombre, url)); // para moder usar DEstino viaje se tiene que importar from './../models/DestinoViaje.model'
  //  console.log(this.destinos);   
   // return false; //false para que no recargue/refresque la pantalla
  //} 
}
