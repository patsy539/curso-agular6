import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from  '../../models/DestinoViaje.model' //'../../models/DestinoViaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';
import { map,filter, debounce, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
fg: FormGroup;
minLongitud = 3;
searchResults: string[];


//forwardRef salva la referencia circular 
constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
  this.onItemAdded = new EventEmitter();

  this.fg = fb.group({
    nombre: ['', Validators.compose ([ /// es una rray de validadores
      Validators.required, // el nombre no puede estar vacio en el modelo
      this.nombreValidator,
      this.nombreValidatorParametrizable(this.minLongitud)
    ]
    )],
    url:  ['']
  });
  this.fg.valueChanges.subscribe(
    (form: any) => {
    console.log('cambio el formulario" ', form);
  });
  // this is new 10/28
  this.fg.controls['nombre'].valueChanges.subscribe(
    (value: string) => {
      console.log('nombre cambio:', value);
    }
  );
}

  ngOnInit() {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
          map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
          filter(text => text.length > 2),
          debounceTime(120),
          distinctUntilChanged(),
          switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text)) // hacemos un ajaz al endpoint
      ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean{
    let d = new DestinoViaje(nombre, url);
    //const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
  nombreValidator(control: FormControl):  { [s: string]: boolean} { // retorna un objeto aquí de tipo booleando
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5){
      return { invalidNombre: true}; // es el objeto que se esta retornando
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => { // es para retornar un string o un null
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong){
        return { minLongNombre: true};
      }
      return null;
    }
  }

}
