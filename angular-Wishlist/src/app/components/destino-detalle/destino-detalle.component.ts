import { Component, OnInit} from '@angular/core'; //InjectionToken, Inject 
import { DestinosApiClient } from '../../models/destinos-api-client.model';  //'./../../models/destinos-api-client.model';
import { DestinoViaje } from '../../models/DestinoViaje.model';
import { ActivatedRoute } from '@angular/router';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
//import { Store } from '@ngrx/store';
//import { AppState } from 'src/app/app.module';

// class DestinosApiClientViejo {
//   getById(id: String): DestinoViaje {
//     console.log('llamando por la calse vieja');
//     return null;
//   }
// }

// interface AppConfig {
//   apiEndpoint: String;
//}

// const APP_CONFIG_VALUE: AppConfig = {
//   apiEndpoint: 'mi_api.com'
// };

// const APP_CONFIG = new InjectionToken<AppConfig>('app.config'); // esta inyectando un valor no un objeto

// class DestinosApiClientDecorated extends DestinosApiClient {
//   constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
//     super(store);
//   }
//   getById(id: String): DestinoViaje{
//     console.log('llamando por la clase decorada');
//     console.log('config:' + this.config.apiEndpoint);
//     return super.getById(id);
//   }
// }

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  template: `
  <mgl-map
    [style]="style"
    [zoom]="[2]"
  >
  </mgl-map>
  `,
  styles: [`
    mgl-map {
      height: 50vh;
      width: 50vw;
    }
  `],
  providers: [DestinosApiClient
    //  { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    //  { provide: DestinosApiClient, useClass: DestinosApiClientDecorated}, // no uses un DestinosApiClient en lugar usa DestinosApiClientDecorated
    //  { provide: DestinosApiClientViejo, useExisting: DestinosApiClient} 
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { } //DestinosApiClientViejo

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
    //const id = this.route.snapshot.paramMap.get('id'); // descomentar
   //this.destino = null; // this.destinosApiClient.getByID(id);
  }

}
